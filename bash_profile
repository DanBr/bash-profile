# Enable tab completion
# For macOS fix at: https://apple.stackexchange.com/a/328144
gitCompletion="$HOME/git-completion.bash"
if [ ! -e $gitCompletion ];
then
    version=$(git --version)
    version=${version##* }
    echo $version
    touch $gitCompletion
    curl https://raw.githubusercontent.com/git/git/v$version/contrib/completion/git-completion.bash > $gitCompletion
fi

source $gitCompletion

source /Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh

# colors!
# https://misc.flogisoft.com/bash/tip_colors_and_formatting
defaultForeground="\[\39mDefault"
green="\[\033[0;32m\]"
lightgreen="\e[92m"
white="\e[97m"
blue="\[\033[0;34m\]"
red="\[\033[0;31m\]"
black="\e[30m\]"
reset="\[\033[0m\]"

# Change command prompt
# Tips&Tricks: http://osxdaily.com/2006/12/11/how-to-customize-your-terminal-prompt/
#source ~/git-prompt.sh
#export GIT_PS1_SHOWDIRTYSTATE=1
# '\u' adds the name of the current user to the prompt
# '\$(__git_ps1)' adds git-related stuff
# '\W' adds the name of the current directory
#export PS1="$red\u$green\$(__git_ps1)$blue \W $ $reset"
# https://misc.flogisoft.com/bash/tip_customize_the_shell_prompt
export PS1="$green\u 🤓$blue \w $red \$(__git_ps1)\n$reset>>"
#export PS1="\[\e[32m\][\[\e[m\]\[\e[31m\]\u\[\e[m\]\[\e[33m\]@\[\e[m\]\[\e[32m\]\h\[\e[m\]:\[\e[36m\]\w\[\e[m\]\[\e[32m\]]\[\e[m\]\[\e[32;47m\]\\$ "
# Add Visual Studio Code (code)
export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"

# color apppearance
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

# alias commands
alias ll='ls -la'

chrome(){
	open -a /Applications/Google\ Chrome.app $1
}
