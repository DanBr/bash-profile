#!bin/sh

workingDir=$(pwd)
sourceFile="$workingDir/bash_profile"
targetLink="~/.bash_profile"

ln -s $sourceFile $targetLink

source $targetLink
